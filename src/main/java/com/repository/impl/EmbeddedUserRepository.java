package com.repository.impl;

import com.dto.UserDto;
import org.springframework.stereotype.Component;
import com.repository.UserRepository;

import java.util.HashMap;
import java.util.Map;

@Component
public class EmbeddedUserRepository implements UserRepository {

    private Map<Integer, UserDto> data = new HashMap<>();

    @Override
    public UserDto findById(final int id) {
        return data.get(id);
    }

    public void save(final UserDto user) {
        final int size = data.size();
        data.put(size, user);
    }
}
