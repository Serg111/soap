package com.repository;

import com.dto.UserDto;

public interface UserRepository {

    UserDto findById(int id);

    void save(UserDto user);
}
