package com.client;

import org.springframework.ws.client.core.WebServiceTemplate;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;

public class WebServiceClient {

    private static final String MESSAGE = "Hello Web Service World";
    private static final String URI = "http://localhost:8080/AnotherWebService";

    private final WebServiceTemplate webServiceTemplate = new WebServiceTemplate();

    public void simpleSendAndReceive() {
        final StreamSource source = new StreamSource(new StringReader(MESSAGE));
        final StreamResult result = new StreamResult(System.out);
        webServiceTemplate.sendSourceAndReceiveToResult(source, result);
    }

    public void customSendAndReceive() {
        final StreamSource source = new StreamSource(new StringReader(MESSAGE));
        final StreamResult result = new StreamResult(System.out);
        webServiceTemplate.sendSourceAndReceiveToResult(URI, source, result);
    }
}
