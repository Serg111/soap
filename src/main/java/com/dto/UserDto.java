package com.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.HashMap;
import java.util.Map;

@XmlType(name = "User", namespace = "http://com.example/soap", propOrder = {"eventMap", "name", "surname", "password"})
public class UserDto extends AbstractDto {

    @XmlElement(required = true)
    private String name;

    @XmlElement(required = true)
    private String surname;

    @XmlElement(required = true)
    private String password;

    private Map<Integer, EventDto> eventMap = new HashMap<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Map<Integer, EventDto> getEventMap() {
        return eventMap;
    }

    public void setEventMap(Map<Integer, EventDto> eventMap) {
        this.eventMap = eventMap;
    }
}
