package com.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;

@XmlType(name = "Event", namespace = "http://com.example/soap")
public class EventDto extends AbstractDto {

    @XmlElement(name = "message", required = true)
    private String msg;

    @XmlElement(required = true)
    private Date date;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
