package com.dto;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "userGetByIdRequest", namespace = "http://com.example/soap")
@XmlType(name = "")
public class UserGetByIdRequest {

    @XmlElement(required = true)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
