package com.dto;

import javax.xml.bind.annotation.*;

@XmlSeeAlso({EventDto.class, UserDto.class})
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(namespace = "http://com.example/soap")
public abstract class AbstractDto {

    @XmlID
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
