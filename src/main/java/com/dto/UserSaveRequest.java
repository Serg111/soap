package com.dto;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "userSaveRequest", namespace = "http://com.example/soap")
@XmlType(name = "")
public class UserSaveRequest {

    @XmlElement
    private UserDto user;

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}
