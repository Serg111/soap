package com.adapter;

import com.dto.EventDto;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EventMapAdapter extends XmlAdapter<EventMapAdapter.AdaptedMap, Map<Integer, EventDto>> {

    static class AdaptedMap {

        private List<Entry> entry = new ArrayList<>();
    }

    static class Entry {

        private Integer key;
        private EventDto value;
    }

    @Override
    public Map<Integer, EventDto> unmarshal(final AdaptedMap adaptedMap) throws Exception {
        final Map<Integer, EventDto> map = new HashMap<>();

        for (final Entry entry : adaptedMap.entry) {
            map.put(entry.key, entry.value);
        }
        return map;
    }

    @Override
    public AdaptedMap marshal(final Map<Integer, EventDto> map) throws Exception {
        final AdaptedMap adaptedMap = new AdaptedMap();

        for (final Map.Entry<Integer, EventDto> mapEntry : map.entrySet()) {
            final Entry entry = new Entry();
            entry.key = mapEntry.getKey();
            entry.value = mapEntry.getValue();
            adaptedMap.entry.add(entry);
        }
        return adaptedMap;
    }

}
