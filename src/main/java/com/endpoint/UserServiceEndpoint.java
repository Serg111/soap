package com.endpoint;

import com.dto.UserGetByIdRequest;
import com.dto.UserGetByIdResponse;
import com.dto.UserSaveRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.repository.UserRepository;

@Endpoint
public class UserServiceEndpoint {

    public static final String SOAP_URI = "http://com.example/soap";

    private UserRepository userRepository;

    @Autowired
    public UserServiceEndpoint(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PayloadRoot(namespace = SOAP_URI, localPart = "userGetByIdRequest")
    @ResponsePayload
    public UserGetByIdResponse getUser(@RequestPayload  final UserGetByIdRequest request) {
        final UserGetByIdResponse response = new UserGetByIdResponse();
        response.setUser(userRepository.findById(request.getId()));

        return response;
    }

    @PayloadRoot(namespace = SOAP_URI, localPart = "userSaveRequest")
    @ResponsePayload
    public void saveUser(@RequestPayload final UserSaveRequest request) {
        userRepository.save(request.getUser());
    }
}
